all : WO
CFLAGS=	-Wall \
		-pedantic \
		-std=c11 \
#		-O2 \
#		-static-libgcc \
#		-lwsock32 \
#		-fpack-struct=4

obj = wo
cc = gcc
WO : $(obj).c
	$(cc) $(obj).c $(CFLAGS) -o $(obj)
	@./$(obj)
	@echo Error : $$?
.PHONY : clean
clean:
	-rm ./$(obj)
